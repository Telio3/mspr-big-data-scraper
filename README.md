# MSPR Big Data Scraper

## Installation

```bash
pnpm install
```

## Production

```bash
pnpm run build

pnpm run start
```

## Development

```bash
pnpm run start:watch
```
