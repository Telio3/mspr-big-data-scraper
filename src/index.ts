import fastify from 'fastify';
import puppeteer from 'puppeteer';
import { Row, Table, Year, Years } from './types';
import * as fs from 'fs';

const server = fastify();

// Définissez une route pour récupérer les tableaux de résultats des élections présidentielles
server.get('/', async (request, reply) => {
    const ys = [1965, 1969, 1974, 1981, 1988, 1995, 2002, 2007, 2012, 2017, 2022];

    const url = 'https://www.sport-histoire.fr/Histoire/Resultats_presidentielle_';

    // Lancez Puppeteer et ouvrez une nouvelle page
    const browser = await puppeteer.launch({ headless: 'new' });
    const page = await browser.newPage();

    try {
        const years: Years = {};

        for (const y of ys) {
            // Accédez à l'URL spécifiée
            await page.goto(`${url}${y}.php`);

            // Attendre que toutes les balises <table> avec la classe "tableau" ou "tableau_gris_centrer" soient présentes
            await page.waitForSelector('table.tableau, table.tableau_gris_centrer');

            // Extraire les données des tableaux
            const data = await page.evaluate(() => {
                const htmlTables = document.querySelectorAll<HTMLTableElement>('table.tableau, table.tableau_gris_centrer');

                const getTable = (htmlTable: HTMLTableElement): Table => {
                    const table: Table = [];

                    htmlTable.querySelectorAll('tr').forEach((tr) => {
                        const row: Row = [];

                        tr.querySelectorAll('th').forEach((th) => {
                            row.push(th.innerText);
                        });

                        tr.querySelectorAll('td').forEach((td) => {
                            row.push(td.innerText);
                        });

                        table.push(row);
                    });

                    return table;
                }

                const year: Year = {
                    1: {
                        votes: getTable(htmlTables[0]),
                        results: getTable(htmlTables[1]),
                    },
                    2: {
                        votes: getTable(htmlTables[2]),
                        results: getTable(htmlTables[3]),
                    },
                };

                return year;
            });

            // Ajoutez les tableaux récupérés à un objet
            if (data) years[y] = data;
        }

        // Fermez le navigateur Puppeteer
        await browser.close();

        // Créez un fichier CSV pour chaque année, tour et type de tableau
        const outputDir = './output';

        if (!fs.existsSync(outputDir)) fs.mkdirSync(outputDir);

        Object.entries(years).forEach(([year, data]) => {
            Object.entries(data).forEach(([round, tables]) => {
                Object.entries(tables).forEach(([type, table]) => {
                    const file = `${outputDir}/${year}_${round}_${type}.csv`;

                    const csv = table.map((row) => row.join(';')).join('\n');

                    if (fs.existsSync(file)) fs.unlinkSync(file);

                    fs.writeFileSync(file, csv);
                });
            });
        });

        reply.send({ message: 'Les tableaux ont été récupérés avec succès.' });
    } catch (error) {
        console.error(error);
        reply.code(500).send({ error: 'Une erreur s\'est produite lors de la récupération des tableaux.' });
    }
});

server.listen({ port: 8080 }, (err, address) => {
    if (err) {
        console.error(err)
        process.exit(1)
    }
});
