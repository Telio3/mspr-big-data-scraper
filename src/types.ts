export type Row = Array<string>;
export type Table = Array<Row>;
type Rounds = Record<'votes' | 'results', Table>;
export type Year = Record<number, Rounds>;
export type Years = Record<number, Year>;
